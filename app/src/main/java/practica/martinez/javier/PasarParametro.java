package practica.martinez.javier;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PasarParametro extends AppCompatActivity {
    EditText cajadatos;
    Button BotonEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);
        cajadatos = (EditText) findViewById(R.id.editTextTextParametro);
        BotonEnviar = (Button) findViewById(R.id.buttonEnviarparametro);
        BotonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PasarParametro.this, RecibirParametro.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato",cajadatos.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}